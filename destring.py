import struct

def NZ(l, crypt, start, inc=6364136223846793005):
    result = ''
    while l >= 0:
        start = (start * inc + 1) & ((2**64)-1)
        decrypted = Dword(crypt + 4*(l>>2)) ^ (start >> 32)
        PatchDword(crypt + 4*(l>>2), decrypted)
        result = struct.unpack('4s', struct.pack('I', decrypted))[0] + result
        l -= 4
    result = result[:result.find('\x00')]
    create_strlit(crypt, BADADDR)
    return result

def AD(ea=None):
    if ea is None: ea = here()
    startea = ea
    assert GetOperandValue(ea, 1) == 6364136223846793005, "Bad starting place"

    while True:
        op = GetOperandValue(ea, 1)
        if op >= 0x400000 and op <= 0x800000:
            break
        ea = prev_head(ea)
        assert ea - startea < 0x50

    strpos = op
    assert get_str_type(strpos) is None
    strea = ea
    while True:
        op = GetOperandValue(ea, 1)
        if op >= 0x100000000:
            break
        ea = next_head(ea)
        assert ea < startea

    seed = op

    ea = next_head(ea)
    ea = next_head(ea)
    op = GetOperandValue(ea, 1)
    assert op > 1
    assert op < 100
    assert GetMnem(next_head(ea)) == 'call'

    l = op - 1

    print NZ(l, strpos, seed)

    Jump(strea)


def ADi(ea=0):
    while True:
        ea = FindImmediate(ea, 1, 6364136223846793005)[0]
        if ea == BADADDR:
            break
        try:
            AD(ea)
        except AssertionError:
            print "Asserted at {:x}".format(ea)
            pass

MOVS = {
    'rax': '',
    'rbx': "\x48\x89\xC3",
    'rcx': "\x48\x89\xC1",
    'rdx': "\x48\x89\xC2",
    'r12': "\x49\x89\xC4",
    'r13': "\x49\x89\xC5",
    'rdi': "\x48\x89\xC7",
}

def AP(ea=None):
    if ea is None: ea = here()
    reg = GetOpnd(ea, 0)
    assert reg in MOVS.keys()
    target = next_head(ea)

    ea = FindImmediate(ea, 0, 6364136223846793005)[0]
    assert target - ea < 0xff0

    while True:
        op = GetOperandValue(ea, 1)
        if op >= 0x400000 and op <= 0x800000:
            break
        ea = prev_head(ea)
        assert target - ea < 0xff0

    assert get_str_type(op) == 0
    assert GetOpnd(ea, 0) == 'rax'

    ea = next_head(ea)
    patch_start = ea

    code = MOVS[reg]
    ea += len (code)
    code += '\xe9' + struct.pack('<I', target - (ea + 5))

    ea = patch_start
    for c in code:
        PatchByte (ea, ord(c))
        ea += 1

    ea = patch_start
    for c in code:
        MakeCode (ea)
        ea += 1

    Jump(patch_start)
